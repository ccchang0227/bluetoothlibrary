//
//  CCCBluetoothManager.swift
//  BluetoothLock
//
//  Created by realtouch's MBP 2019 on 2019/11/4.
//  Copyright © 2019 林脩得. All rights reserved.
//

import UIKit
import CoreBluetooth

enum CCCBluetoothError: LocalizedError {
    case cannotConnectToDevice
    case deviceConnected
    
    case cannotDisconnectFromDevice
    case deviceDisconnected
    
    var errorDescription: String? {
        switch self {
        case .cannotConnectToDevice:
            return NSLocalizedString("Could not connect to device", comment: "")
        case .deviceConnected:
            return NSLocalizedString("Device connected", comment: "")
        case .cannotDisconnectFromDevice:
            return NSLocalizedString("Could not disconnect from device", comment: "")
        case .deviceDisconnected:
            return NSLocalizedString("Device disconnected", comment: "")
        }
    }
    
}

class CCCBluetoothManager: NSObject, CBCentralManagerDelegate {
    public static let shared = CCCBluetoothManager()
    
    public static let peripheralDidDisconnectNotification = Notification.Name("CCCBluetoothManagerPeripheralDidDisconnectNotificationName")
    public static let disconnectedPeripheralKey = "CCCBluetoothManagerDisconnectedPeripheralKey"
    public static let disconnectReasonKey = "CCCBluetoothManagerDisconnectReasonKey"
    
    override init() {
        super.init()
        
        self.centerManager.delegate = self
        
    }
    
    // MARK: - Property
    
    fileprivate(set) var centerManager: CBCentralManager = {
        let options = [CBCentralManagerOptionShowPowerAlertKey : true]
        return CBCentralManager(delegate: nil, queue: nil, options: options)
    }()
    
    @objc
    fileprivate(set) var discoveredPeripherals: [CCCBluetoothPeripheral] = []
    @objc
    fileprivate(set) var connectedPeripherals: [CCCBluetoothPeripheral] = []

    @objc
    fileprivate(set) var isBluetoothOn: Bool = false
    
    fileprivate var isKeepScaning = false
    
    fileprivate var customErrors: [UUID : Error] = [:]
    
    // MARK: -
    
    public func startScan() {
        self.isKeepScaning = true
        self._startScan()
    }
    
    fileprivate func _startScan() {
        guard self.centerManager.state == .poweredOn else {
            return
        }
        
        let options = [CBCentralManagerScanOptionAllowDuplicatesKey : true]
        self.centerManager.scanForPeripherals(withServices: nil, options: options)
        
        self._startTimeoutTimer()
    }
    
    public func stopScan() {
        self.isKeepScaning = false
        self._stopScan()
    }
    
    fileprivate func _stopScan() {
        self._stopTimeoutTimer()
        self.centerManager.stopScan()
        
        self.willChangeValue(forKey: "discoveredPeripherals")
        self.discoveredPeripherals.removeAll()
        self.didChangeValue(forKey: "discoveredPeripherals")
    }
    
    // 檢查藍牙掃描time out
    fileprivate var timeoutTimer: DispatchSourceTimer?
    fileprivate func _startTimeoutTimer() {
        if self.timeoutTimer != nil {
            self.timeoutTimer?.cancel()
        }
        
        self.timeoutTimer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
        self.timeoutTimer?.schedule(deadline: DispatchTime.now(), repeating: .seconds(3), leeway: .milliseconds(10))
        self.timeoutTimer?.setEventHandler {
            
            let currentTimestamp = Date().timeIntervalSince1970
            let timeoutPeripherals: [CCCBluetoothPeripheral] = self.discoveredPeripherals.compactMap {
                if let timestamp = $0.timestamp, currentTimestamp - timestamp > 3.0 {
                    return $0
                }
                
                return nil
            }
            if timeoutPeripherals.count > 0 {
                self.willChangeValue(forKey: "discoveredPeripherals")
                self.discoveredPeripherals.removeAll { timeoutPeripherals.contains($0) }
                self.didChangeValue(forKey: "discoveredPeripherals")
            }
            
        }
        self.timeoutTimer?.setCancelHandler {
            self.timeoutTimer = nil
        }
        self.timeoutTimer?.resume()
        
    }
    
    fileprivate func _stopTimeoutTimer() {
        if self.timeoutTimer != nil {
            self.timeoutTimer?.cancel()
        }
    }
    
    public func connectedCCCBluetoothPeripheral(with peripheral: CBPeripheral) -> CCCBluetoothPeripheral? {
        let filteredArray = self.connectedPeripherals.filter { $0.identifier == peripheral.identifier }
        if filteredArray.count != 0 {
            return filteredArray.first
        }
        
        return nil
    }

    // MARK: - 連線＆斷線
    
    public func connect(to peripheral: CCCBluetoothPeripheral, timeout: TimeInterval = 10, completion: CCCBluetoothCompletionBlock?) {
        
        if let identifier = peripheral.identifier {
            self.customErrors.removeValue(forKey: identifier)
        }
        
        guard let cbPeripheral = peripheral.peripheral else {
            if let completion = completion {
                completion(CCCBluetoothError.cannotConnectToDevice)
            }
            return
        }
        guard !self.connectedPeripherals.contains(peripheral) else {
            if let completion = completion {
                completion(CCCBluetoothError.deviceConnected)
            }
            return
        }
        
        peripheral.completionHandler = completion
        
        self.willChangeValue(forKey: "connectedPeripherals")
        self.connectedPeripherals.append(peripheral)
        self.didChangeValue(forKey: "connectedPeripherals")
        
        self.willChangeValue(forKey: "discoveredPeripherals")
        self.discoveredPeripherals.removeAll { $0 == peripheral }
        self.didChangeValue(forKey: "discoveredPeripherals")
        
        let options = [CBConnectPeripheralOptionNotifyOnConnectionKey : true,
                       CBConnectPeripheralOptionNotifyOnDisconnectionKey : true]
        self.centerManager.connect(cbPeripheral,
                                   options: options)
        
        if timeout > 0 {
            // Start connection timeout timer
            self.perform(#selector(connectionTimeout(_:)), with: peripheral, afterDelay: timeout)
        }
        
    }

    @objc
    fileprivate func connectionTimeout(_ peripheral: CCCBluetoothPeripheral) {
        guard let cbPeripheral = peripheral.peripheral else {
            return
        }
        
        if cbPeripheral.state != .connected {
            self.connectionTimeoutAndDisconnect(peripheral, error: CCCBluetoothError.cannotConnectToDevice)
        }
        
    }
    
    public func disconnect(_ peripheral: CCCBluetoothPeripheral, completion: CCCBluetoothCompletionBlock?) {
        guard let cbPeripheral = peripheral.peripheral else {
            if let completion = completion {
                completion(CCCBluetoothError.cannotDisconnectFromDevice)
            }
            
            if let identifier = peripheral.identifier {
                self.customErrors.removeValue(forKey: identifier)
            }
            return
        }
        guard self.connectedPeripherals.contains(peripheral) else {
            if let completion = completion {
                completion(CCCBluetoothError.deviceDisconnected)
            }
            
            if let identifier = peripheral.identifier {
                self.customErrors.removeValue(forKey: identifier)
            }
            return
        }
        
        peripheral.completionHandler = completion
        
        self.centerManager.cancelPeripheralConnection(cbPeripheral)
    }
    
    fileprivate func connectionTimeoutAndDisconnect(_ peripheral: CCCBluetoothPeripheral, error: Error?) {
        
        if let error = error, let identifier = peripheral.identifier {
            self.customErrors[identifier] = error
        }
        
        self.disconnect(peripheral, completion: peripheral.completionHandler)
    }
    
    // MARK: - CBCentralManagerDelegate
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("state: \(central.state)")
        
        switch central.state {
        case .poweredOn:
            if self.isKeepScaning {
                self._startScan()
            }
            
            self.willChangeValue(forKey: "isBluetoothOn")
            self.isBluetoothOn = true
            self.didChangeValue(forKey: "isBluetoothOn")
            
        case .resetting:
            fallthrough
        case .unsupported:
            fallthrough
        case .unauthorized:
            fallthrough
        case .poweredOff:
            fallthrough
        default:
            self.willChangeValue(forKey: "isBluetoothOn")
            self.isBluetoothOn = false
            self.didChangeValue(forKey: "isBluetoothOn")
            
            self._stopScan()
            
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        print("didDiscover: \(peripheral)")
        print("didDiscover: \(advertisementData)")
        print("didDiscover: \(RSSI)")
        print("-----------------------------------------")
        
        self.willChangeValue(forKey: "discoveredPeripherals")
        
        let bluetoothPeripheral: CCCBluetoothPeripheral = self.discoveredPeripherals.filter {
            $0.identifier == peripheral.identifier
        }.first ?? self.connectedPeripherals.filter {
            $0.identifier == peripheral.identifier
        }.first ?? {
            let peripheral = CCCBluetoothPeripheral()
            self.discoveredPeripherals.append(peripheral)
            return peripheral
        }()
        
        if let name = peripheral.name, name.count != 0 {
            bluetoothPeripheral.deviceName = peripheral.name
        }
        else {
            let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String
            bluetoothPeripheral.deviceName = name
        }
        
        bluetoothPeripheral.identifier = peripheral.identifier
        bluetoothPeripheral.peripheral = peripheral
        bluetoothPeripheral.advertisementData = advertisementData
        bluetoothPeripheral.rssi = RSSI
        bluetoothPeripheral.timestamp = Date().timeIntervalSince1970
        
        self.didChangeValue(forKey: "discoveredPeripherals")
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnectPeripheral: \(peripheral)")
        print("-----------------------------------------")
        
        guard let connectedPeripheral = self.connectedCCCBluetoothPeripheral(with: peripheral) else {
            return
        }
        
        if let completionHandler = connectedPeripheral.completionHandler {
            completionHandler(nil)
        }
        connectedPeripheral.completionHandler = nil
        
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("didFailToConnectPeripheral: \(peripheral)")
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
        
        guard let connectedPeripheral = self.connectedCCCBluetoothPeripheral(with: peripheral) else {
            return
        }
        
        if let completionHandler = connectedPeripheral.completionHandler {
            completionHandler(error)
        }
        connectedPeripheral.completionHandler = nil
        
        self.willChangeValue(forKey: "connectedPeripherals")
        self.connectedPeripherals.removeAll { $0 == connectedPeripheral }
        self.didChangeValue(forKey: "connectedPeripherals")
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("didDisconnectPeripheral: \(peripheral)")
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
        
        guard let connectedPeripheral = self.connectedCCCBluetoothPeripheral(with: peripheral) else {
            return
        }
        
        let customError = self.customErrors[peripheral.identifier]
        
        if let completionHandler = connectedPeripheral.completionHandler {
            if let error = error {
                completionHandler(error)
            }
            else if let error = customError {
                completionHandler(error)
            }
            else {
                completionHandler(nil)
            }
        }
        connectedPeripheral.completionHandler = nil
        
        var userInfo: [String : Any] = [:]
        userInfo[CCCBluetoothManager.disconnectedPeripheralKey] = connectedPeripheral
        if let error = error {
            userInfo[CCCBluetoothManager.disconnectReasonKey] = error
        }
        else if let error = customError {
            userInfo[CCCBluetoothManager.disconnectReasonKey] = error
        }
        
        self.customErrors.removeValue(forKey: peripheral.identifier)
        
        self.willChangeValue(forKey: "connectedPeripherals")
        self.connectedPeripherals.removeAll { $0 == connectedPeripheral }
        self.didChangeValue(forKey: "connectedPeripherals")
        
        let notification = Notification(name: CCCBluetoothManager.peripheralDidDisconnectNotification,
                                        object: nil,
                                        userInfo: userInfo)
        NotificationCenter.default.post(notification)
        
    }
    
    // MARK: - KVO
    
    override class func automaticallyNotifiesObservers(forKey key: String) -> Bool {
        if key == "connectedPeripherals" {
            return false
        }
        
        if key == "discoveredPeripherals" {
            return false
        }
        
        return super.automaticallyNotifiesObservers(forKey: key)
    }
    
}
