//
//  CCCBluetoothPeripheral.swift
//  BluetoothLock
//
//  Created by realtouch's MBP 2019 on 2019/11/4.
//  Copyright © 2019 林脩得. All rights reserved.
//

import UIKit
import CoreBluetooth

typealias CCCBluetoothCompletionBlock = ((Error?) -> Void)

class CCCBluetoothPeripheral: NSObject, Codable {
    
    var deviceName: String?
    var identifier: UUID?
    var advertisementData: [String : Any]?
    var rssi: NSNumber?
    var peripheral: CBPeripheral?
    var timestamp: TimeInterval?
    
    var manufacturerName: String?
    var serialNumber: String?
    var firmwareRevision: String?
    var hardwareRevision: String?
    
    var completionHandler: CCCBluetoothCompletionBlock?
    
    fileprivate enum CodingKeys: String, CodingKey {
        case deviceName
        case identifier
        case manufacturerName
        case serialNumber
        case firmwareRevision
        case hardwareRevision
    }
    
}
