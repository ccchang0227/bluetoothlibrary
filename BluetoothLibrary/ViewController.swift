//
//  ViewController.swift
//  BluetoothLibrary
//
//  Created by realtouch's MBP 2019 on 2019/11/4.
//  Copyright © 2019 RealTouchApp Corp. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DeviceInfoViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = true
        
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        }
        
    }
    
    // MARK: - View lifecycle ♻️

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startDisplayLink()
        self.bluetoothManager.startScan()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.bluetoothManager.stopScan()
        self.stopDisplayLink()
        
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if identifier == "ShowDeviceInfo" && sender is UITableViewCell {
            guard let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell) else {
                return false
            }
            guard indexPath.row >= 0 && indexPath.row < self.discoveredPeripherals.count else {
                return false
            }
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if sender is UITableViewCell && segue.identifier == "ShowDeviceInfo" {
            guard let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell) else {
                return
            }
            guard indexPath.row >= 0 && indexPath.row < self.discoveredPeripherals.count else {
                return
            }
            guard let deviceInfoViewController = segue.destination as? DeviceInfoViewController else {
                return
            }
            
            deviceInfoViewController.delegate = self
            deviceInfoViewController.peripheral = self.discoveredPeripherals[indexPath.row]
            
        }
        else if segue.identifier == "PushConnected" {
            guard let deviceViewController = segue.destination as? ConnectedDeviceViewController else {
                return
            }
            guard let peripheral = sender as? CCCBluetoothPeripheral else {
                return
            }
            
            deviceViewController.peripheral = peripheral
            
        }
        
    }
    
    // MARK: - Property
    
    fileprivate let bluetoothManager = CCCBluetoothManager.shared
    
    fileprivate var discoveredPeripherals: [CCCBluetoothPeripheral] {
        return self.bluetoothManager.discoveredPeripherals
    }
    
    // MARK: - CADisplayLink

    fileprivate var displayLink: CADisplayLink?
    fileprivate func startDisplayLink() {
        self.displayLink = CADisplayLink(target: self, selector: #selector(handleDisplayLink(_:)))
        self.displayLink?.frameInterval = 30 //30=每秒執行2次
        self.displayLink?.add(to: .current, forMode: .default)
    }
    
    @objc
    fileprivate func handleDisplayLink(_ displayLink: CADisplayLink) {
        self.tableView.reloadData()
    }
    
    fileprivate func stopDisplayLink() {
        self.displayLink?.invalidate()
        self.displayLink = nil
    }
    
    // MARK: -
    
    fileprivate func configure(_ cell: TableViewCell, at indexPath: IndexPath) {
        guard indexPath.row >= 0 && indexPath.row < self.discoveredPeripherals.count else {
            return
        }
        
        let peripheral = self.discoveredPeripherals[indexPath.row]
        if peripheral.deviceName != nil && peripheral.deviceName?.count != 0 {
            cell.label1.text = peripheral.deviceName
        }
        else {
            cell.label1.text = "Unknown"
        }
        
        cell.label2.text = peripheral.identifier?.uuidString
        
        if let rssi = peripheral.rssi {
            cell.label3.text = "RSSI：\(rssi)"
        }
        else {
            cell.label3.text = "RSSI："
        }
        
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.discoveredPeripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        self.configure(cell, at: indexPath)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - DeviceInfoViewControllerDelegate
    
    func peripheralDidConnect(_ viewController: DeviceInfoViewController, peripheral: CCCBluetoothPeripheral) {
        viewController.dismiss(animated: false, completion: nil)
        
        self.performSegue(withIdentifier: "PushConnected", sender: peripheral)
    }
    
    // MARK: - Orientation
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
}

