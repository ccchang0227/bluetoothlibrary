//
//  DeviceInfoViewController.swift
//  BluetoothLibrary
//
//  Created by realtouch's MBP 2019 on 2019/11/4.
//  Copyright © 2019 RealTouchApp Corp. Ltd. All rights reserved.
//

import UIKit

protocol DeviceInfoViewControllerDelegate {
    func peripheralDidConnect(_ viewController: DeviceInfoViewController, peripheral: CCCBluetoothPeripheral)
}

class DeviceInfoViewController: UIViewController {

    var delegate: DeviceInfoViewControllerDelegate?
    
    var peripheral: CCCBluetoothPeripheral?
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        }
        
        self.update()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startDisplayLink()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.stopDisplayLink()
        
    }
    
    // MARK: -

    fileprivate var timer: DispatchSourceTimer?
    fileprivate func startDisplayLink() {
        self.timer = DispatchSource.makeTimerSource(flags: [], queue: .main)
        self.timer?.schedule(deadline: DispatchTime.now(), repeating: .seconds(2), leeway: .milliseconds(10))
        self.timer?.setEventHandler {
            
            self.update()
            
        }
        self.timer?.setCancelHandler {
            self.timer = nil
        }
        self.timer?.resume()
        
    }
    
    fileprivate func stopDisplayLink() {
        self.timer?.cancel()
    }
    
    fileprivate func update() {
        self.titleLabel.text = self.peripheral?.deviceName
        
        var text = ""
        if let uuid = self.peripheral?.identifier {
            text += "UUID: \(uuid.uuidString)\n"
        }
        if let rssi = self.peripheral?.rssi {
            text += "RSSI: \(rssi)\n"
        }
        if let advData = self.peripheral?.advertisementData {
            do {
                var validJSONFormat: [String : Any] = [:]
                for (key, value) in advData {
                    if JSONSerialization.isValidJSONObject(value) {
                        validJSONFormat[key] = value
                    }
                    else {
                        if value is [String : Any] {
                            var newDic: [String: String] = [:]
                            for (k, v) in value as! [String : Any] {
                                newDic[k] = "\(v)"
                            }
                            validJSONFormat[key] = newDic
                        }
                        else if value is [Any] {
                            var array: [String] = []
                            for i in value as! [Any] {
                                array.append("\(i)")
                            }
                            validJSONFormat[key] = array
                        }
                        else {
                            validJSONFormat[key] = "\(value)"
                        }
                    }
                }
                
                if JSONSerialization.isValidJSONObject(validJSONFormat) {
                    let jsonData = try JSONSerialization.data(withJSONObject: validJSONFormat, options: .prettyPrinted)
                    if let json = String(data: jsonData, encoding: .utf8) {
                        text += "advertisementData:\n\(json)\n"
                    }
                    else {
                        text += "advertisementData:\n\(advData)\n"
                    }
                }
                else {
                    text += "advertisementData:\n\(advData)\n"
                }
                
            } catch {
                print("\(error)")
                
                text += "advertisementData:\n\(advData)\n"
            }
            
        }
        self.textView.text = text
        
    }
    
    // MARK: - Actions
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func connect(_ sender: Any) {
        guard let peripheral = self.peripheral else {
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.backgroundColor = UIColor(white: 0, alpha: 0.6)
        hud.label.text = "Connecting"
        
        CCCBluetoothManager.shared.connect(to: peripheral, timeout: 15) { (error) in
            
            if let error = error {
                hud.mode = .text
                hud.label.text = error.localizedDescription
                hud.hide(animated: true, afterDelay: 1.5)
                
            }
            else {
                hud.mode = .text
                hud.label.text = "Successfully Connected"
                hud.hide(animated: true, afterDelay: 1.0)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.delegate?.peripheralDidConnect(self, peripheral: peripheral)
                }
                
            }
            
        }
        
    }
    
    // MARK: - Orientation
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
}
