//
//  ConnectedDeviceViewController.swift
//  BluetoothLibrary
//
//  Created by realtouch's MBP 2019 on 2019/11/5.
//  Copyright © 2019 RealTouchApp Corp. Ltd. All rights reserved.
//

import UIKit
import CoreBluetooth

class ConnectedDeviceViewController: UIViewController, CBPeripheralDelegate {
    
    var peripheral: CCCBluetoothPeripheral?
    
    var services: [CBService]?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    
    @IBOutlet weak var disconnectButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        }
        
        self.rssiLabel.text = nil
        
        if let peripheral = self.peripheral, let cbPeripheral = peripheral.peripheral {
            self.titleLabel.text = peripheral.deviceName
            
            cbPeripheral.delegate = self
            
            if cbPeripheral.services == nil {
                cbPeripheral.discoverServices(nil)
            }
            else {
                self.services = cbPeripheral.services
                for service in self.services! {
                    if service.characteristics == nil {
                        cbPeripheral.discoverCharacteristics(nil, for: service)
                    }
                }
            }
            
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deviceDidDisconnect(_:)),
                                               name: CCCBluetoothManager.peripheralDidDisconnectNotification,
                                               object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: CCCBluetoothManager.peripheralDidDisconnectNotification,
                                                  object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startTimer()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.stopTimer()
        
    }
    
    // MARK: -
    
    fileprivate var timer: DispatchSourceTimer?
    fileprivate func startTimer() {
        self.timer = DispatchSource.makeTimerSource(flags: [], queue: .main)
        self.timer?.schedule(deadline: DispatchTime.now(), repeating: .seconds(1), leeway: .milliseconds(10))
        self.timer?.setEventHandler {
            
            if let peripheral = self.peripheral, let cbPeripheral = peripheral.peripheral {
                cbPeripheral.readRSSI()
            }
            else {
                self.stopTimer()
            }
            
        }
        self.timer?.setCancelHandler {
            self.timer = nil
        }
        self.timer?.resume()
        
    }
    
    fileprivate func stopTimer() {
        self.timer?.cancel()
    }
    
    // MARK: - Actions
    
    fileprivate var isDisconnectManually = false
    
    @IBAction func disconnect(_ sender: Any) {
        guard let peripheral = self.peripheral else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.isDisconnectManually = true
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.backgroundColor = UIColor(white: 0, alpha: 0.6)
        hud.label.text = "Disconnecting"
        
        CCCBluetoothManager.shared.disconnect(peripheral) { (error) in
            
            if let error = error {
                hud.mode = .text
                hud.label.text = error.localizedDescription
                hud.hide(animated: true, afterDelay: 1.5)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.isDisconnectManually = false
                }
            }
            else {
                hud.mode = .text
                hud.label.text = "Successfully Disconnected"
                hud.hide(animated: true, afterDelay: 1.0)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.isDisconnectManually = false
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            
        }
        
    }
    
    // MARK: - Notification
    
    @objc
    fileprivate func deviceDidDisconnect(_ notification: Notification) {
        let peripheral = notification.userInfo?[CCCBluetoothManager.disconnectedPeripheralKey] as? CCCBluetoothPeripheral
        guard peripheral != nil && peripheral == self.peripheral else {
            return
        }
        guard !self.isDisconnectManually else {
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.backgroundColor = UIColor(white: 0, alpha: 0.6)
        hud.mode = .text
        hud.label.text = "Device disconnected"
        if let name = peripheral?.deviceName {
            hud.label.text = (hud.label.text ?? "") + ": \(name)"
        }
        
        if let error = notification.userInfo?[CCCBluetoothManager.disconnectReasonKey] as? Error {
            hud.detailsLabel.text = error.localizedDescription
        }
        
        hud.hide(animated: true, afterDelay: 1.5)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - CBPeripheralDelegate
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {

        CCCBluetoothManager.shared.willChangeValue(forKey: "connectedPeripherals")
        self.peripheral?.rssi = RSSI
        CCCBluetoothManager.shared.didChangeValue(forKey: "connectedPeripherals")
        
        self.rssiLabel.text = "RSSI: \(RSSI)"
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("didDiscoverServices: \(peripheral)")
        if peripheral.services != nil {
            print("services: \(peripheral.services!)")
        }
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
        
        self.services = peripheral.services
        if let services = self.services {
            for service in services {
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("didDiscoverCharacteristicsForService: \(peripheral)")
        print("service: \(service)")
        if service.characteristics != nil {
            print("characteristic: \(service.characteristics!)")
        }
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
        
        // 裝置基本資訊
        if service.uuid.uuidString == "180A", let characteristics = service.characteristics {
            for characteristic in characteristics {
                if characteristic.properties.contains(.read) {
                    // 如果具備"讀"特性，即可以讀取特性的value
                    peripheral.readValue(for: characteristic)
                }
            }
            return
        }
        
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateValueForCharacteristic: \(peripheral)")
        print("characteristic: \(characteristic)")
        
        var valueString: String?
        if let value = characteristic.value {
            print("value: \(value)")
            if let valueString = String(data: value, encoding: .utf8) {
                print("Receive -> \(valueString)")
            }
            valueString = String(data: value, encoding: .utf8)
        }
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
        
        if characteristic.uuid.uuidString == "2A29" {
            self.peripheral?.manufacturerName = valueString
        }
        else if characteristic.uuid.uuidString == "2A25" {
            self.peripheral?.serialNumber = valueString
        }
        else if characteristic.uuid.uuidString == "2A26" {
            self.peripheral?.firmwareRevision = valueString
        }
        else if characteristic.uuid.uuidString == "2A27" {
            self.peripheral?.hardwareRevision = valueString
        }
        else {
            // TODO: parse value from other characteristic
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didWriteValueForCharacteristic: \(peripheral)")
        print("characteristic: \(characteristic)")
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateNotificationStateForCharacteristic: \(peripheral)")
        print("characteristic: \(characteristic)")
        print("error: \(String(describing: error))")
        print("-----------------------------------------")
    }
    
    // MARK: - Orientation
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
}
