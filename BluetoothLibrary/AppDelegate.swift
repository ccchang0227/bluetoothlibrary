//
//  AppDelegate.swift
//  BluetoothLibrary
//
//  Created by realtouch's MBP 2019 on 2019/11/4.
//  Copyright © 2019 RealTouchApp Corp. Ltd. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

